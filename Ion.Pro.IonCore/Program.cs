﻿using Ion.Data.Networking.CoreCom;
using Ion.Data.Networking.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ion.Pro.IonCore
{
    class Program
    {
        public const string Version = "0.0.1 alpha";
        static void Main(string[] args)
        {
            Console.Title = "Ion Core";
            LogManagerServer logServer = new LogManagerServer();
            logServer.OnVerbose += LogServer_OnVerbose;
            CommandListner listener = new CommandListner(new Commands(logServer.CreateLogger("CORECOM"), new SystemCore(logServer)));
            listener.Start();


            string line = "";
            while ((line = Console.ReadLine()) != "exit")
            {
            }
        }

        private static void LogServer_OnVerbose(object sender, LogWriteEventArgs e)
        {
            Console.WriteLine($"{e.Entry.Time} {e.Entry.Sender} {e.Entry.Level} {e.Entry.Value}");
        }
    }

    public class Commands
    {
        SystemCore core;
        ILogger logger;
        public Commands(ILogger logger, SystemCore core)
        {
            this.logger = logger;
            this.core = core;
        }
        public string Version()
        {
            return Program.Version;
        }

        public string Update(string updateFile)
        {
            core.UpdateManager.EnqueUpdate(updateFile);
            return "OK";
        }

        public string Register(string name, string id)
        {
            int idVal;
            if (int.TryParse(id, out idVal))
            {
                core.ProcessManager.Register(name, idVal);
                return "OK";
            }
            else
            {
                logger.WriteException("Process: " + name + " dose not contain an value", new Exception("Id not an number"), 3);
                return "NO ID";
            }

        }
    }

    public class SystemCore
    {
        public UpdateManager UpdateManager { get; private set; }// = new UpdateManager();
        public ProcessManager ProcessManager { get; private set; }// = new ProcessManager();

        public SystemCore(ILogManager manager)
        {
            UpdateManager = new UpdateManager(manager.CreateLogger("UPMAN"));
            ProcessManager = new ProcessManager(manager.CreateLogger("PROMAN"));
        }
    }

    public class ProcessManager
    {
        ILogger logger;
        public List<ProcessInformation> Processes { get; } = new List<ProcessInformation>();

        public ProcessManager(ILogger logger)
        {
            this.logger = logger;
        }

        public void Register(string name, int id)
        {
            Processes.Add(new ProcessInformation(id, name));
            logger.WriteInfo("Registered process: " + name + " with id: " + id.ToString());
        }
    }

    public class ProcessInformation
    {
        public int RunningId { get; set; }
        public string Name { get; set; }
        public Process ProcessObj => Process.GetProcessById(RunningId);

        public ProcessInformation(int id, string Name)
        {
            this.RunningId = id;
        }
            
    }

    public class UpdateManager
    {
        string currentPath = null;
        private ILogger logger;

        public UpdateManager(ILogger logInterface)
        {
            this.logger = logInterface;
        }

        public void EnqueUpdate(string updateFile)
        {
            logger.WriteInfo("Enqueing update");
            currentPath = updateFile;
            Task.Run(new Action(Update));
        }

        public void Update()
        {
            System.Threading.Thread.Sleep(500);
            
            if (currentPath != null)
            {
                logger.WriteInfo("Starting update");
                FileInfo fi = new FileInfo(currentPath);
                Stream s = fi.OpenRead();
                ZipArchive archive = new ZipArchive(s);
                ZipArchiveEntry entry = archive.GetEntry("update.conf");
                TextReader tr = new StreamReader(entry.Open());
                logger.WriteInfo("Getting update info");
                string path = tr.ReadLine();
                string startProcess = tr.ReadLine();
                tr.Close();
                logger.WriteInfo("Extracting files");
                /*if (Directory.Exists(path))
                    Directory.Delete(path, true);
                Directory.CreateDirectory(path);*/
                archive.ExtractToDirectory(path);
                s.Close();
                logger.WriteInfo("Starting process");
                ProcessStartInfo info = new ProcessStartInfo(Path.Combine(path, startProcess));
                info.WorkingDirectory = path;
                Process.Start(info);
                logger.WriteInfo("Update complete");
            }
        }
    }
}
